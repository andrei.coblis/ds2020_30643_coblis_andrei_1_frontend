import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import "./App.css";
import LoginForm from "./components/loginForm";
import NavBar from "./components/navBar";
import RegisterForm from "./components/registerForm";
import UserForm from "./components/userForm";
import Logout from "./components/logout";
import Users from "./components/users";
import Patients from "./components/patients";
import Doctors from "./components/doctors";
import Caregivers from "./components/caregivers";
import Medications from "./components/medications";
import MedicationPlans from "./components/medicationPlans";
import PatientForm from "./components/patientForm";
import CaregiverForm from "./components/caregiverForm";
import DoctorForm from "./components/doctorForm";
import MedicationForm from "./components/medicationForm";
import MedicationPlanForm from "./components/medicationPlanForm";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <NavBar />
        <main className="container">
          <Switch>
            <Route exact path="/login" component={LoginForm} />
            <Route exact path="/register" component={RegisterForm} />
            <Route exact path="/logout" component={Logout} />
            <Route exact path="/users/:id" component={UserForm} />
            <Route exact path="/users" component={Users} />
            <Route exact path="/patients/:id" component={PatientForm} />
            <Route exact path="/patients" component={Patients} />
            <Route exact path="/doctors/:id" component={DoctorForm} />
            <Route exact path="/doctors" component={Doctors} />
            <Route exact path="/caregivers/:id" component={CaregiverForm} />
            <Route exact path="/caregivers" component={Caregivers} />
            <Route exact path="/medications/:id" component={MedicationForm} />
            <Route exact path="/medications" component={Medications} />
            <Route
              exact
              path="/medicationPlans/:id"
              component={MedicationPlanForm}
            />
            <Route exact path="/medicationPlans" component={MedicationPlans} />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
