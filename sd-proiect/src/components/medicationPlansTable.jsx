import React, { Component } from "react";
//import auth from "../services/authService";
import { Link } from "react-router-dom";
import Table from "./common/table";

class MedicationPlansTable extends Component {
  columns = [
    {
      path: "patientName",
      label: "PatientName",
      content: medicationPlan => <Link to={`/medicationPlans/${medicationPlan.id}`}>{medicationPlan.patientName}</Link>
    },
    { path: "startDate", label: "StartDate" },
    { path: "endDate", label: "EndDate" },
  ];

  deleteColumn = {
    key: "delete",
    content: medicationPlan => (
      <button
        onClick={() => this.props.onDelete(medicationPlan)}
        className="btn btn-danger btn-sm"
      >
        Delete
      </button>
    )
  };

  constructor() {
    super();
    //const user = auth.getCurrentUser();
    //if (user && user.isAdmin) this.columns.push(this.deleteColumn);
    this.columns.push(this.deleteColumn);
  }

  render() {
    const { medicationPlans, onSort, sortColumn } = this.props;

    return (
      <Table
        columns={this.columns}
        data={medicationPlans}
        sortColumn={sortColumn}
        onSort={onSort}
      />
    );
  }
}

export default MedicationPlansTable;
