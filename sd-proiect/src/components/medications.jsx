import React, { Component } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import Pagination from "./common/pagination";
import MedicationsTable from "./medicationsTable";
import { getMedications, deleteMedication } from "../services/medicationService";
import { paginate } from "../utils/paginate";
import _ from "lodash";
import SearchBox from "./searchBox";

class Medications extends Component {
    state = {
        medications: [],
        currentPage: 1,
        pageSize: 4,
        searchQuery: "",
        sortColumn: { path: "name", order: "asc" }
    }

    async componentDidMount() {
        const { data: medications } = await getMedications();
        this.setState({ medications });
    }

    handleDelete = async medication => {
        const originalMedications = this.state.medications;
        const medications = originalMedications.filter(u => u.id !== medication.id);
        this.setState({ medications });
    
        try {
          await deleteMedication(medication.id);
        } catch (ex) {
          if (ex.response && ex.response.status === 404)
            toast.error("This medication has already been deleted.");
    
          this.setState({ medications: originalMedications });
        }
    };

    handlePageChange = page => {
        this.setState({ currentPage: page });
    };

    handleSearch = query => {
        this.setState({ searchQuery: query, currentPage: 1 });
    };

    handleSort = sortColumn => {
        this.setState({ sortColumn });
    };

    getPagedData = () => {
        const {
          pageSize,
          currentPage,
          sortColumn,
          searchQuery,
          medications: allMedications
        } = this.state;
    
        let filtered = allMedications;
        if (searchQuery)
          filtered = allMedications.filter(u =>
            u.name.toLowerCase().startsWith(searchQuery.toLowerCase())
          );
    
        const sorted = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);
    
        const medications = paginate(sorted, currentPage, pageSize);
    
        return { totalCount: filtered.length, data: medications };
    };

    render() {
        const { length: count } = this.state.medications;
        const { pageSize, currentPage, sortColumn, searchQuery } = this.state;
        //const { user } = this.props;
    
        if (count === 0) return <p>There are no medications in the database.</p>;
    
        const { totalCount, data: medications } = this.getPagedData();
    
        return (
          <div className="row">
            <div className="col">
              {
                <Link
                  to="/medications/new"
                  className="btn btn-primary"
                  style={{ marginBottom: 20 }}
                >
                  New Medication
                </Link>
              }
              <p>Showing {totalCount} medications in the database.</p>
              <SearchBox value={searchQuery} onChange={this.handleSearch} />
              <MedicationsTable
                medications={medications}
                sortColumn={sortColumn}
                onDelete={this.handleDelete}
                onSort={this.handleSort}
              />
              <Pagination
                itemsCount={totalCount}
                pageSize={pageSize}
                currentPage={currentPage}
                onPageChange={this.handlePageChange}
              />
            </div>
          </div>
        );
    }

}
export default Medications;