import React from "react";
import Joi from "joi-browser";
import Form from "./common/form";
import { getUser, saveUser } from "../services/userService";

class UserForm extends Form {
  state = {
    data: {
      username: "",
      password: "",
      role: "",
      firstName: "",
      lastName: "",
      address: "",
      gender: "",
      birthDate: ""
    },
    errors: {}
  };

  schema = {
    _id: Joi.number(),
    username: Joi.string()
      .required()
      .label("Username"),
    password: Joi.string()
      .required()
      .label("Password"),
    role: Joi.string()
      .required()
      .label("Role"),
    firstName: Joi.string()
      .required()
      .label("firstName"),
    lastName: Joi.string()
      .required()
      .label("Lastname"),
    address: Joi.string()
      .required()
      .label("Address"),
    gender: Joi.string()
      .required()
      .label("Gender"),
    birthDate: Joi.date()
      .required()
      .label("Birthdate"),
  };

  async populateUser() {
    try {
      const userId = this.props.match.params.id;
      if (userId === "new") return;

      const { data: user } = await getUser(userId);
      this.setState({ data: this.mapToViewModel(user) });
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        this.props.history.replace("/not-found");
    }
  }

  async componentDidMount() {
    await this.populateUser();
  }

  mapToViewModel(user) {
    return {
      _id: user.userId,
      username: user.username,
      password: user.password,
      role: user.role,
      firstName: user.firstName,
      lastName: user.lastName,
      address: user.address,
      gender: user.gender,
      birthDate: user.birthDate
    };
  }

  doSubmit = async () => {
    await saveUser(this.state.data);

    this.props.history.push("/users");
  };

  render() {
    return (
      <div>
        <h1>User Form</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("username", "Username")}
          {this.renderInput("password", "Password")}
          {this.renderInput("role", "Role")}
          {this.renderInput("firstName", "Firstname")}
          {this.renderInput("lastName", "Lastname")}
          {this.renderInput("address", "Address")}
          {this.renderInput("gender", "Gender")}
          {this.renderInput("birthDate", "Birthdate")}
          {this.renderButton("Save")}
        </form>
      </div>
    );
  }
}

export default UserForm;
