import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/patients";

function patientUrl(id) {
  return `${apiEndpoint}/${id}`;
}

export function getPatients() {
  return http.get(apiEndpoint);
}

export function getPatient(id) {
  return http.get(patientUrl(id));
}

export function savePatient(patient) {
  if (patient.id) {
    const body = { ...patient };
    delete body.id;
    return http.put(patientUrl(patient.id), body);
  }

  return http.post(apiEndpoint, patient);
}

export function deletePatient(id) {
  return http.delete(patientUrl(id));
}
